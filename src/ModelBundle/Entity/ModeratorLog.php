<?php

namespace ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModeratorLog
 *
 * @ORM\Table(name="moderator_log", indexes={@ORM\Index(name="FK_moderator_log_moderator_action", columns={"action"}), @ORM\Index(name="FK_moderator_log_moderator", columns={"mod_id"}), @ORM\Index(name="FK_moderator_log_moderator_2", columns={"mod2_id"})})
 * @ORM\Entity
 */
class ModeratorLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts", type="datetime", nullable=false)
     */
    private $ts;

    /**
     * @var integer
     *
     * @ORM\Column(name="review_id", type="integer", nullable=false)
     */
    private $reviewId;

    /**
     * @var integer
     *
     * @ORM\Column(name="hotel_id", type="integer", nullable=false)
     */
    private $hotelId;

    /**
     * @var \Moderator
     *
     * @ORM\ManyToOne(targetEntity="Moderator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mod_id", referencedColumnName="id")
     * })
     */
    private $mod;

    /**
     * @var \Moderator
     *
     * @ORM\ManyToOne(targetEntity="Moderator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mod2_id", referencedColumnName="id")
     * })
     */
    private $mod2;

    /**
     * @var \ModeratorAction
     *
     * @ORM\ManyToOne(targetEntity="ModeratorAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="action", referencedColumnName="id")
     * })
     */
    private $action;

    /**
     * @var boolean
     *
     * @ORM\Column(name="processed", type="boolean", nullable=false)
     */
    private $processed;

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getId() ?: '-';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ts
     *
     * @param \DateTime $ts
     *
     * @return ModeratorLog
     */
    public function setTs($ts)
    {
        $this->ts = $ts;

        return $this;
    }

    /**
     * Get ts
     *
     * @return \DateTime
     */
    public function getTs()
    {
        return $this->ts;
    }

    /**
     * Set reviewId
     *
     * @param integer $reviewId
     *
     * @return ModeratorLog
     */
    public function setReviewId($reviewId)
    {
        $this->reviewId = $reviewId;

        return $this;
    }

    /**
     * Get reviewId
     *
     * @return integer
     */
    public function getReviewId()
    {
        return $this->reviewId;
    }

    /**
     * Set hotelId
     *
     * @param integer $hotelId
     *
     * @return ModeratorLog
     */
    public function setHotelId($hotelId)
    {
        $this->hotelId = $hotelId;

        return $this;
    }

    /**
     * Get hotelId
     *
     * @return integer
     */
    public function getHotelId()
    {
        return $this->hotelId;
    }

    /**
     * Set mod
     *
     * @param \ModelBundle\Entity\Moderator $mod
     *
     * @return ModeratorLog
     */
    public function setMod(\ModelBundle\Entity\Moderator $mod = null)
    {
        $this->mod = $mod;

        return $this;
    }

    /**
     * Get mod
     *
     * @return \ModelBundle\Entity\Moderator
     */
    public function getMod()
    {
        return $this->mod;
    }

    /**
     * Set mod2
     *
     * @param \ModelBundle\Entity\Moderator $mod2
     *
     * @return ModeratorLog
     */
    public function setMod2(\ModelBundle\Entity\Moderator $mod2 = null)
    {
        $this->mod2 = $mod2;

        return $this;
    }

    /**
     * Get mod2
     *
     * @return \ModelBundle\Entity\Moderator
     */
    public function getMod2()
    {
        return $this->mod2;
    }

    /**
     * Set action
     *
     * @param \ModelBundle\Entity\ModeratorAction $action
     *
     * @return ModeratorLog
     */
    public function setAction(\ModelBundle\Entity\ModeratorAction $action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return \ModelBundle\Entity\ModeratorAction
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set processed
     *
     * @param boolean $processed
     *
     * @return ModeratorLog
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * Get processed
     *
     * @return boolean
     */
    public function getProcessed()
    {
        return $this->processed;
    }
}
