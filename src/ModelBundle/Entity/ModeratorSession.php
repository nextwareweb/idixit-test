<?php

namespace ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModeratorSession
 *
 * @ORM\Table(name="moderator_session")
 * @ORM\Entity(repositoryClass="ModelBundle\Repository\ModeratorSessionRepository")
 */
class ModeratorSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Moderator
     *
     * @ORM\ManyToOne(targetEntity="Moderator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mod_id", referencedColumnName="id")
     * })
     */
    private $mod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="session_date", type="datetime")
     */
    private $sessionDate;

    /**
     * @var int
     *
     * @ORM\Column(name="actions", type="integer")
     */
    private $actions;

    /**
     * @var int
     *
     * @ORM\Column(name="worked", type="integer")
     */
    private $worked;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float")
     */
    private $cost;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getId() ?: '-';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sessionDate
     *
     * @param \DateTime $sessionDate
     *
     * @return ModeratorSession
     */
    public function setSessionDate($sessionDate)
    {
        $this->sessionDate = $sessionDate;

        return $this;
    }

    /**
     * Get sessionDate
     *
     * @return \DateTime
     */
    public function getSessionDate()
    {
        return $this->sessionDate;
    }

    /**
     * Set worked
     *
     * @param integer $worked
     *
     * @return ModeratorSession
     */
    public function setWorked($worked)
    {
        $this->worked = $worked;

        return $this;
    }

    /**
     * Get worked
     *
     * @return integer
     */
    public function getWorked()
    {
        return $this->worked;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return ModeratorSession
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set mod
     *
     * @param \ModelBundle\Entity\Moderator $mod
     *
     * @return ModeratorSession
     */
    public function setMod(\ModelBundle\Entity\Moderator $mod = null)
    {
        $this->mod = $mod;

        return $this;
    }

    /**
     * Get mod
     *
     * @return \ModelBundle\Entity\Moderator
     */
    public function getMod()
    {
        return $this->mod;
    }

    /**
     * Set cost
     *
     * @param float $cost
     *
     * @return ModeratorSession
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set actions
     *
     * @param integer $actions
     *
     * @return ModeratorSession
     */
    public function setActions($actions)
    {
        $this->actions = $actions;

        return $this;
    }

    /**
     * Get actions
     *
     * @return integer
     */
    public function getActions()
    {
        return $this->actions;
    }
}
