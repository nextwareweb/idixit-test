<?php

namespace ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Moderator
 *
 * @ORM\Table(name="moderator")
 * @ORM\Entity
 */
class Moderator
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="mod_id", type="integer", unique=true)
     */
    private $modId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=100, nullable=true)
     */
    private $login;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=10, scale=0, nullable=true)
     */
    private $cost;

    /**
     * @var string
     *
     * @ORM\Column(name="main_language", type="string", length=5, nullable=true)
     */
    private $mainLanguage;

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName() ?: '-';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Moderator
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set cost
     *
     * @param float $cost
     *
     * @return Moderator
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set mainLanguage
     *
     * @param string $mainLanguage
     *
     * @return Moderator
     */
    public function setMainLanguage($mainLanguage)
    {
        $this->mainLanguage = $mainLanguage;

        return $this;
    }

    /**
     * Get mainLanguage
     *
     * @return string
     */
    public function getMainLanguage()
    {
        return $this->mainLanguage;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Moderator
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set modId
     *
     * @param integer $modId
     *
     * @return Moderator
     */
    public function setModId($modId)
    {
        $this->modId = $modId;

        return $this;
    }

    /**
     * Get modId
     *
     * @return integer
     */
    public function getModId()
    {
        return $this->modId;
    }
}
