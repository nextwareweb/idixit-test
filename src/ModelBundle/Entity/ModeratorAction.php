<?php

namespace ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModeratorAction
 *
 * @ORM\Table(name="moderator_action")
 * @ORM\Entity
 */
class ModeratorAction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comment_en", type="string", length=256, nullable=false)
     */
    private $commentEn;

    /**
     * @var string
     *
     * @ORM\Column(name="summary_en", type="string", length=256, nullable=false)
     */
    private $summaryEn;

    /**
     * @var integer
     *
     * @ORM\Column(name="spend", type="integer", nullable=false)
     */
    private $spend;

    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getSummaryEn() ?: '-';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commentEn
     *
     * @param string $commentEn
     *
     * @return ModeratorAction
     */
    public function setCommentEn($commentEn)
    {
        $this->commentEn = $commentEn;

        return $this;
    }

    /**
     * Get commentEn
     *
     * @return string
     */
    public function getCommentEn()
    {
        return $this->commentEn;
    }

    /**
     * Set summaryEn
     *
     * @param string $summaryEn
     *
     * @return ModeratorAction
     */
    public function setSummaryEn($summaryEn)
    {
        $this->summaryEn = $summaryEn;

        return $this;
    }

    /**
     * Get summaryEn
     *
     * @return string
     */
    public function getSummaryEn()
    {
        return $this->summaryEn;
    }

    /**
     * Set spend
     *
     * @param integer $spend
     *
     * @return ModeratorAction
     */
    public function setSpend($spend)
    {
        $this->spend = $spend;

        return $this;
    }

    /**
     * Get spend
     *
     * @return integer
     */
    public function getSpend()
    {
        return $this->spend;
    }
}
