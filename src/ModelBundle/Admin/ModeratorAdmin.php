<?php

namespace ModelBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ModeratorAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->with('Content', array('class' => 'col-md-9'))
            ->add('id')
            ->add('modId')
            ->add('name')
            ->add('login')
            ->add('cost')
            ->add('mainLanguage')
            ->end()

            ->with('Meta data', array('class' => 'col-md-3'))
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('modId')
            ->add('name')
            ->add('login')
            ->add('mainLanguage');

    }

    protected function configureListFields(ListMapper $list)
    {

        $list->addIdentifier('id', null, [
            'header_style' => 'width: 5%; text-align: center',
            'row_align' => 'center'
        ])
            ->add('modId')
            ->add('name')
            ->add('login')
            ->add('cost')
            ->add('mainLanguage')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        return $instance;
    }
}

