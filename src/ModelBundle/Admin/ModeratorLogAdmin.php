<?php

namespace ModelBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ModeratorLogAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->with('Content', array('class' => 'col-md-9'))
            ->add('reviewId')
            ->add('hotelId')
            ->add('mod')
            ->add('mod2')
            ->add('action')
            ->end()

            ->with('Meta data', array('class' => 'col-md-3'))
                ->add('ts', 'sonata_type_datetime_picker', ['format' => 'dd/MM/y HH:mm:ss', 'required' => true])
                ->add('processed')
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('reviewId')
            ->add('hotelId')
            ->add('mod')
            ->add('action')
            ->add('processed');

    }

    protected function configureListFields(ListMapper $list)
    {

        $list->addIdentifier('id', null, [
            'header_style' => 'width: 5%; text-align: center',
            'row_align' => 'center'
        ])
            ->add('reviewId')
            ->add('hotelId')
            ->add('mod')
            ->add('mod2')
            ->add('action')
            ->add('ts')
            ->add('processed')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        return $instance;
    }
}

