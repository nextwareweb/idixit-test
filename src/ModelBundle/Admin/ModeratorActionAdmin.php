<?php

namespace ModelBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ModeratorActionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->with('Content', array('class' => 'col-md-9'))
            ->add('id')
            ->add('commentEn')
            ->add('summaryEn')
            ->add('spend')
            ->end()

            ->with('Meta data', array('class' => 'col-md-3'))
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('commentEn')
            ->add('summaryEn');

    }

    protected function configureListFields(ListMapper $list)
    {

        $list->addIdentifier('id', null, [
            'header_style' => 'width: 5%; text-align: center',
            'row_align' => 'center'
        ])
            ->add('commentEn')
            ->add('summaryEn')
            ->add('spend')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        return $instance;
    }
}

