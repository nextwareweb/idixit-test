<?php

namespace ModelBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ModeratorSessionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->with('Content', array('class' => 'col-md-9'))
            ->add('mod')
            ->add('actions')
            ->add('worked')
            ->add('cost')
            ->add('total')
            ->end()

            ->with('Meta data', array('class' => 'col-md-3'))
            ->add('sessionDate', 'sonata_type_datetime_picker', ['format' => 'dd/MM/y HH:mm:ss', 'required' => true])
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('mod')
            ->add('sessionDate')
            ->add('worked');

    }

    protected function configureListFields(ListMapper $list)
    {

        $list->addIdentifier('id', null, [
            'header_style' => 'width: 5%; text-align: center',
            'row_align' => 'center'
        ])
            ->add('mod')
            ->add('sessionDate')
            ->add('actions')
            ->add('worked')
            ->add('cost')
            ->add('total')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        return $instance;
    }
}

