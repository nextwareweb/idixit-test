<?php

namespace ScriptBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateSessionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('idixit:update:sessions')
            ->setDescription('Update moderator sessions')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* Config */
        $maxTimeToCloseSession = 60 * 30;

        $output->writeln('Updating moderator sessions...');
        $em = $this->getContainer()->get('doctrine')->getEntityManager();

        /* Get not processed logs */
        $logs = $this->query('SELECT m.id as mod_id, UNIX_TIMESTAMP(ml.ts) as ts, m.cost as cost
               FROM moderator_log ml
               LEFT JOIN moderator m ON (ml.mod_id = m.id)
               WHERE ml.processed = false
               ORDER BY m.id ASC, ml.ts ASC;'
            , []);

        $output->writeln('Get ('.count($logs).') unprocessed logs.');

        /* Parse logs and store sessions */
        $sessions = [];
        $actualSession = ['ts' => 0, 'lines' => 0, 'worked' => 0, 'cost' => 0];
        $actualMod = 0;
        $maxTS = 0;

        foreach ($logs as $log) {
            $mod = $log['mod_id'];
            $ts = $log['ts'];
            $cost = $log['cost'];
            $maxTS = $ts > $maxTS ? $ts : $maxTS;

            if ($actualMod != $mod) {
                /* Close session */
                if ($actualSession['lines'] > 0) {
                    $sessions[$actualMod][] = $actualSession;
                    $output->writeln('* add session: '.$actualSession['worked'].'s.');
                }

                $output->writeln('Processing moderator: '.$mod);

                /* Reset values */
                $actualSession = ['ts' => 0, 'lines' => 0, 'worked' => 0, 'cost' => $cost];
                $actualMod = $mod;
            }

            if ($actualSession['ts'] == 0) {
                $actualSession['ts'] = $ts;
                $actualSession['lines'] = 1;
            } else {
                $diff = $ts - $actualSession['ts'];
                if ($diff > $maxTimeToCloseSession) {
                    /* Close session */
                    if ($actualSession['lines'] > 0) {
                        $sessions[$actualMod][] = $actualSession;
                        $output->writeln('* add session: '.$actualSession['worked'].'s.');
                    }

                    /* Reset values */
                    $actualSession = ['ts' => $ts, 'lines' => 1, 'worked' => 0, 'cost' => $cost];
                    $actualMod = $mod;
                } else {
                    $actualSession['ts'] = $ts;
                    $actualSession['lines']++;
                    $actualSession['worked'] += $diff;
                }
            }
        }

        /* Remain log process */
        if ($actualSession['lines'] > 0) {
            $sessions[$actualMod][] = $actualSession;
            $output->writeln('* add session: '.$actualSession['worked'].'s.');
        }

        /* Store sessions in DB */
        foreach ($sessions as $mod => $session) {
            $lines = 0;
            $total = 0;

            foreach ($session as $entry) {
                $lines++;
                $total += $entry['worked'];
                $this->addSessionDB($mod, $entry['ts'], $entry['lines'], $entry['worked'], $entry['cost']);
            }

            $output->writeln('Stored in DB: mod_id='.$mod.' num_sessions='.$lines.' total_time='.$total.'s');
        }

        /* Update Moderator Logs to process lines */
        $this->query('UPDATE moderator_log ml SET ml.processed = true WHERE ml.ts <= FROM_UNIXTIME(?);', [$maxTS], false);
    }

    private function addSessionDB($mod_id, $sesion_date, $actions, $worked, $cost) {
        /* calc total*/
        $total = $worked * ($cost / (60*60));

        $this->query('INSERT INTO `moderator_session` (`mod_id`,`session_date`,`actions`,`worked`,`cost`,`total`)
          VALUES (?, FROM_UNIXTIME(?), ?, ?, ?, ?);', [
            $mod_id, $sesion_date, $actions, $worked, $cost, $total
        ], false);
    }

    private function query($sql, $params = [], $return = true) {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $conn = $em->getConnection();
        $stmt = $conn->prepare($sql);

        foreach($params as $key => $value) {
            $key++;
            if (is_integer($value)) {
                $stmt->bindValue($key, $value, \PDO::PARAM_INT);
            } else {
                $stmt->bindValue($key, $value);
            }

        }

        $stmt->execute();
        if ($return) return $stmt->fetchAll();
    }
}
