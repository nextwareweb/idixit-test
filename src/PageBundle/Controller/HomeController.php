<?php

namespace PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomeController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('@Page/Home/home.html.twig');
    }

    /**
     * @Route("/demo")
     */
    public function demoAction()
    {
        return $this->render('@Page/Home/demo.html.twig');
    }

    /**
     * @Route("/page/dashboard")
     */
    public function pageDashboardAction()
    {
        return $this->render('@Page/Page/dashboard.html.twig');
    }

    /**
     * @Route("/page/costByLanguage")
     */
    public function costByLanguageAction()
    {
        return $this->render('@Page/Page/statistic-costByLanguage.html.twig');
    }

    /**
     * @Route("/page/averageCostByAction")
     */
    public function averageCostByActionAction()
    {
        return $this->render('@Page/Page/statistic-averageCostByAction.html.twig');
    }

    /**
     * @Route("/page/costByModerator")
     */
    public function costByModeratorAction()
    {
        return $this->render('@Page/Page/statistic-costByModerator.html.twig');
    }
}
