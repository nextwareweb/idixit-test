<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use ApiBundle\Base\ApiRestController;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Symfony\Component\EventDispatcher\EventDispatcher,
    Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken,
    Symfony\Component\Security\Core\Authentication\Token\AnonymousToken,
    Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class ModeratorController extends ApiRestController
{
    /**
     * Get the total cost by language (month)
     * @RequestParam(name="year", requirements="\w+", nullable=false, default="")
     *
     * @return Array
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @ApiDoc(
     *      section="Statistics",
     *      statusCodes={
     *          200 = "Returned when successful",
     *          404 = "Return when user or password not found"
     *      }
     * )
     *
     * @View()
     * @Get("/api/statistics/costByLanguage")
     */
    public function costByLanguageAction(Request $request)
    {
        $response = [];
        $year = $request->get('year') ?: date("Y");

        $result = $this->query('SELECT MONTH(ms.session_date) as month, m.main_language as language,
             COUNT(ms.id) as sessions, SUM(ms.total) as cost
          FROM moderator_session ms
          LEFT JOIN moderator m ON (ms.mod_id = m.id)
          WHERE YEAR(ms.session_date) = ?
          GROUP BY m.main_language, MONTH(ms.session_date)
          ORDER BY MONTH(ms.session_date), m.main_language'
          , [$year]);

        /* Format result */
        foreach ($result as $item) {
            $language = $item['language'];
            $month = $item['month'];

            /* If not exist language in response, add it */
            if (!isset($response[$language])) {
                $response[$language] = [
                    'name' => $language,
                    'month' => [],
                    'total' => 0
                ];

                for ($i=1; $i<=12; $i++) {
                    $response[$language]['month'][$i] = 0;
                }
            }

            $response[$language]['month'][$month] = number_format((float)$item['cost'], 2, '.', '');
        }

        /* Get the total by language */
        foreach ($response as $lang => $values) {
            $response[$lang]['total'] = number_format((float)array_sum($values['month']), 2, '.', '');
        }

        return $this->response->send($response);
    }

    /**
     * Get the average cost by action (language)
     * @RequestParam(name="year", requirements="\w+", nullable=false, default="")
     *
     * @return Array
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @ApiDoc(
     *      section="Statistics",
     *      statusCodes={
     *          200 = "Returned when successful",
     *          404 = "Return when user or password not found"
     *      }
     * )
     *
     * @View()
     * @Get("/api/statistics/averageCostByAction")
     */
    public function averageCostByActionAction(Request $request)
    {
        $response = [];
        $year = $request->get('year') ?: date("Y");

        $result = $this->query('SELECT m.main_language as language,SUM(ms.actions) as actions, SUM(ms.total) as cost,
              (SUM(ms.total) / SUM(ms.actions)) as average
            FROM moderator_session ms
            LEFT JOIN moderator m ON (ms.mod_id = m.id)
            WHERE YEAR(ms.session_date) = ?
            GROUP BY m.main_language
            ORDER BY m.main_language ASC'
            , [$year]);

        foreach ($result as $row) {
            $response[$row['language']] = [
                'language' => $row['language'],
                'actions' => $row['actions'],
                'cost' => number_format((float)$row['cost'], 2, '.', ''),
                'average' => number_format((float)$row['average'], 4, '.', '')
            ];
        }

        return $this->response->send($response);
    }

    /**
     * Get the cost by moderator (by month)
     * @RequestParam(name="year", requirements="\w+", nullable=false, default="")
     *
     * @return Array
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @ApiDoc(
     *      section="Statistics",
     *      statusCodes={
     *          200 = "Returned when successful",
     *          404 = "Return when user or password not found"
     *      }
     * )
     *
     * @View()
     * @Get("/api/statistics/costByModerator")
     */
    public function costByModeratorAction(Request $request)
    {
        $response = [];
        $year = $request->get('year') ?: date("Y");

        $result = $this->query('SELECT m.id as mod_id, m.name as mod_name, MONTH(ms.session_date) as month,
              SUM(ms.actions) as actions, SUM(ms.total) as cost
            FROM moderator_session ms
            LEFT JOIN moderator m ON (ms.mod_id = m.id)
            WHERE YEAR(ms.session_date) = ?
            GROUP BY m.id, MONTH(ms.session_date)
            ORDER BY m.name ASC'
            , [$year]);

        /* Format result */
        foreach ($result as $item) {
            $modId = $item['mod_id'];
            $modName = $item['mod_name'];
            $month = $item['month'];

            /* If not exist language in response, add it */
            if (!isset($response[$modId])) {
                $response[$modId] = [
                    'name' => $modName,
                    'month' => [],
                    'total' => 0
                ];

                for ($i=1; $i<=12; $i++) {
                    $response[$modId]['month'][$i] = 0;
                }
            }

            $response[$modId]['month'][$month] = number_format((float)$item['cost'], 2, '.', '');
        }

        /* Get the total by language */
        foreach ($response as $lang => $values) {
            $response[$lang]['total'] = number_format((float)array_sum($values['month']), 2, '.', '');
        }

        return $this->response->send($response);
    }
}
