<?php

namespace ApiBundle\Base;

use FOS\RestBundle\Controller\FOSRestController;

/**
 * Class ApiRestController
 * @package ApiRestBundle\Base
 */
abstract class ApiRestController extends FOSRestController
{
    protected $response = null;

    function __construct() {
        $this->response = new ApiRestResponse();
    }

    function implode($array) {
        return is_array($array) ? json_encode($array) : $array;
    }

    function explode($string) {
        if (is_array($string)) return $string;
        if (!$string = trim($string)) return null;

        if ($this->isJson($string)) {
            return json_decode($string);
        } else {
            return explode(',', $string);
        }
    }

    function query($sql, $params = []) {
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $stmt = $conn->prepare($sql);

        foreach($params as $key => $value) {
            $key++;
            if (is_integer($value)) {
                $stmt->bindValue($key, $value, \PDO::PARAM_INT);
            } else {
                $stmt->bindValue($key, $value);
            }

        }

        $stmt->execute();
        return $stmt->fetchAll();
    }

    private function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
