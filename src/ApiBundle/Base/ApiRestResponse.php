<?php

namespace ApiBundle\Base;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use \Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiRestController
 * @package ApiRestBundle\Base
 */
class ApiRestResponse
{
    private $user = null;

    private $code = 0;
    private $errors = [];
    private $response = null;
    private $error_messages = [];

    function __construct()
    {
        /* Load Error messages */
        $error_messages = Yaml::parse(file_get_contents(__DIR__.'/../Resources/config/error_messages.yml'));
        foreach ($error_messages as $message) {
            $this->error_messages[$message[0]] = $message[1];
        }
    }

    /**
     * Define Api Call and check permissions needed to call method.
     * @param $protocol
     * @param $method
     * @param User $user
     * @param array $roles
     */
    public function setup($protocol, $method, $user = null, $roles = []) {
        $user_roles = $user ? $user->getRoles() : [];
        $this->user = $user;
        $this->response['header']['method'] = strtoupper($protocol).' '.$method;

        /* Check permissions */
        if ($roles && !is_array($roles))
            $roles = [$roles];

        if ($roles && count($roles)) {
            foreach ($roles as $role) {
                if (in_array($role, $user_roles))
                    return true;
            }

            /* User hasn't permission to do this action */
            $this->error(403, 1001, 'privileges', null, true);
        }
    }

    /**
     * Add an error to response data
     * @param $num
     * @param $type
     * @param $message
     */
    public function error($code, $err_num, $type, $fields = [], $critical = false)
    {
        /**
         * Type definition:
         *   privileges: User can't do the requested action
         *   integrity: Related to call integrity (entities, parameters, etc)
         *   field: Related to form fields (name, email, etc)
         */

        $this->response['header']['code'] = $code;
        $this->response['header']['description'] = $this->error_messages[$code];

        $error = [
            'error' => $err_num,
            'type' => $type,
            'message' => $this->error_messages[$err_num]
        ];

        if ($fields && count($fields)) {
            $error['fields'] = $fields;
        }

        $this->errors[] = $error;

        if ($critical) {
            $this->submitErrors();
        }
    }

    /**
     * If response have errors, then return and finish, else continue
     */
    public function submitErrors() {
        /* check if have errors */
        if (count($this->errors)) {
            $this->response['errors'] = [
                'count' => count($this->errors),
                'items' => $this->errors
            ];

            header('Access-Control-Allow-Origin: *');

            print json_encode($this->send());
            die();
        }
    }

    /**
     * Add the response result and return the error (if has) or the response.
     * @param null $values
     */
    public function send($values = null, $map = null, $extra_data = [])
    {
        /* check if have errors */
        if (count($this->errors)) {
            $this->response['errors'] = [
                'count' => count($this->errors),
                'items' => $this->errors
            ];

            return $this->response;
        }

        /* else */
        $this->response['header']['code'] = 200;
        $this->response['header']['description'] = 'ok';
        $this->response['header']['timestamp'] = time();

        $values = (is_array($values)) ? $values : [$values];
        $parsed_values = [];

        /* Format values */
        $parse = (isset(getallheaders()['parse'])) ? getallheaders()['parse'] : true;
        if ($map && $parse) {
            foreach ($values as $key => $value) {
                $parsed_values[] = $this->_mapEntity($value, $map);
            }
        } else {
            $parsed_values = $values;
        }

        $this->response['response'] = [
            'count' => count($values),
            'items' => $parsed_values
        ];

        if (is_array($extra_data) && count($extra_data)) {
            $this->response['response']['data'] = $extra_data;
        }

        return new Response(json_encode($this->response));
    }

    private function _mapEntity($object, $map)
    {
        $result = [];
        if (is_null($object)) return null;

        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($map as $map_key => $map_value) {
            if (is_array($map_value)) {
                /* Check if is a secuential array */
                if (count($map_value) == 1 && isset($map_value['*'])) {
                    $temp = [];
                    foreach ($accessor->getValue($object, $map_key) as $item_object) {
                        $temp[] = $this->_mapEntity($item_object, $map_value['*']);
                    }
                    $result[$map_key] = $temp;
                } else {
                    $temp = $accessor->getValue($object, $map_key);
                    $temp = $this->_mapEntity($temp, $map_value);
                    $result[$map_key] = $temp;
                }
            } else {
                $temp = $accessor->getValue($object, $map_value);
                $result[$map_value] = $temp;
            }
        }

        return $result;
    }
}