Instructions for deploy:

1. Clone the git repository
2. Create the database and dump the backup.sql (structure is diferent that the source sql files)
3. Make writtable some folders:

   $ chmod 777 -R var/cache var/session var/logs

4. Refresh symfony cache

   $ bin/console cache:clear

5. Execute composer:

   $ composer update

6. Run sessions cron:

   $ bin/console idixit:update:sessions