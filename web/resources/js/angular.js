((function($) {
    var root_path = window.location.pathname+window.location.search;
    var api_path = root_path + 'api/';

    angular.module("main", ["ngRoute"])
        .config(function($routeProvider){
            $routeProvider
                .when("/", {
                    controller: "homeCtrl",
                    controllerAs: "vm",
                    templateUrl: root_path + "page/dashboard",
                    title: 'Dashboard'
                })
                .when("/dashboard", {
                    controller: "homeCtrl",
                    controllerAs: "vm",
                    templateUrl: root_path + "page/dashboard",
                    title: 'Dashboard'
                })
                .when("/statistic/costByLanguage", {
                    controller: "homeCtrl",
                    controllerAs: "vm",
                    templateUrl: root_path + "page/costByLanguage",
                    title: 'Total cost by language'
                })
                .when("/statistic/averageCostByAction", {
                    controller: "homeCtrl",
                    controllerAs: "vm",
                    templateUrl: root_path + "page/averageCostByAction",
                    title: 'Average cost by action'
                })
                .when("/statistic/costByModerator", {
                    controller: "homeCtrl",
                    controllerAs: "vm",
                    templateUrl: root_path + "page/costByModerator",
                    title: 'Total cost by moderator'
                })

            ;
        })

        .controller("homeCtrl", function($rootScope, $scope){
            /* Change Page Title */
            $rootScope.$on("$routeChangeSuccess", function(event, currentRoute, previousRoute) {
                $('#page-wrapper > .row').fadeIn();
                $rootScope.title = currentRoute.title;
            });
        })

        .controller("dashboardCtrl", function($scope, $http){

        })

        .controller("costByLanguageCtrl", function($scope, $http){
            $http({
                method: 'GET',
                url: api_path + 'statistics/costByLanguage?year=2017'
            }).then(function (response){
                $scope.costByLanguage = response['data']['response']['items'];
            });
        })

        .controller("averageCostByActionCtrl", function($scope, $http){
            $scope.languageKeys = ['cat', 'de', 'en', 'es', 'fr', 'it', 'nl', 'pt', 'ru', 'da', 'sv'];

            $http({
                method: 'GET',
                url: api_path + 'statistics/averageCostByAction?year=2017'
            }).then(function (response){
                $scope.averageCostByAction = response['data']['response']['items'];
            });
        })

        .controller("costByModeratorCtrl", function($scope, $http){
            $http({
                method: 'GET',
                url: api_path + 'statistics/costByModerator?year=2017'
            }).then(function (response){
                $scope.costByModerator = response['data']['response']['items'];
            });
        })
    ;



    $(document).ready(function() {

    });
})(jQuery));